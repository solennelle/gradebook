using System;
using Xunit;

namespace GradeBook.Tests
{
    public class TypeTests
    {
        [Fact]
        public void GetBookReturnsDifferentObjects()
        {
            // setup
            var book1 = GetBook("Book 1");
            var book2 = GetBook("Book 2");

            // execution

            // assert

            Assert.Equal("Book 1", book1.Name);
            Assert.Equal("Book 2", book2.Name);
        }

        private Book GetBook(string name)
        {
            return new Book(name);
        }

        [Fact]
        public void TwoVariablesCanReferenceSameObject()
        {
            // setup
            var book1 = GetBook("Book 1");
            var book2 = book1;

            // execution

            // assert

            Assert.Same(book1, book2);
            Assert.True(ReferenceEquals(book1, book2));
        }

        [Fact]
        public void StringsBehaveLikeValueTypes()
        {
            // setup
            const string parameter = "Scott";
            var upper = MakeUpperCase(parameter);

            // execution

            // assert
            Assert.Equal("Scott", parameter);
            Assert.Equal("SCOTT", upper);
        }

        private string MakeUpperCase(string parameter)
        {
            return parameter.ToUpper();
        }
    }
}

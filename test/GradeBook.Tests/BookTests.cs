using System;
using Xunit;

namespace GradeBook.Tests
{
    public class GradeBookTests
    {
        [Fact]
        public void GetCorrectStatisticsTest()
        {
            // setup
            // put together test data
            var book = new Book("");
            book.AddGrade(89.1);
            book.AddGrade(90.5);
            book.AddGrade(77.3);


            // execution
            var result = book.GetStatistics();

            // assert
            Assert.Equal(85.6, result.Average, 1);
            Assert.Equal(90.5, result.HighGrade, 1);
            Assert.Equal(77.3, result.LowGrade, 1);
        }

        [Fact]
        public void OnlyAddValidValuesTest()
        {
            // setup
            var book = new Book("");
            book.AddGrade(99);
            book.AddGrade(150);
            book.AddGrade(-20);

            // execution
            var result = book.GetStatistics();

            // assert
            Assert.Equal(99, result.Average, 1);
            Assert.Equal(99, result.HighGrade, 1);
            Assert.Equal(99, result.LowGrade, 1);
        }

        [Fact]
        public void GetCorrectGradesTest()
        {
            // setup
            var bookA = new Book("");
            bookA.AddGrade(99);
            bookA.AddGrade(95);

            var bookB = new Book("");
            bookB.AddGrade(88);
            bookB.AddGrade(82);

            var bookC = new Book("");
            bookC.AddGrade(75);
            bookC.AddGrade(71);

            var bookD = new Book("");
            bookD.AddGrade(61);
            bookD.AddGrade(69);

            var bookF = new Book("");
            bookF.AddGrade(45);
            bookF.AddGrade(50);

            // execution
            var resultA = bookA.GetStatistics();
            var resultB = bookB.GetStatistics();
            var resultC = bookC.GetStatistics();
            var resultD = bookD.GetStatistics();
            var resultF = bookF.GetStatistics();

            // assert
            Assert.Equal("A", resultA.Letter);
            Assert.Equal("B", resultB.Letter);
            Assert.Equal("C", resultC.Letter);
            Assert.Equal("D", resultD.Letter);
            Assert.Equal("F", resultF.Letter);
        }
    }
}

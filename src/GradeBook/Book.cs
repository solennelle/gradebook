﻿using System;
using System.Collections.Generic;
using System.IO;

namespace GradeBook
{
    //objects fit anything (str, int...)
    public delegate void GradeAddedDelegate(object sender, EventArgs args);

    public class NamedObject {
        public NamedObject(string name)
        {
            Name = name;
        }

        public string Name {
            get;
            set;
        }
    }

    public interface IBook {
        void AddGrade(double grade);
        Statistics GetStatistics();
        string Name {get;}
        event GradeAddedDelegate GradeAdded;
    }

    public abstract class Book : NamedObject, IBook {
        protected Book(string name) : base(name)
        {
        }

        public abstract event GradeAddedDelegate GradeAdded;
        public abstract void AddGrade(double grade);
        public abstract Statistics GetStatistics();
    }

    public class InMemoryBook : Book
    {
        public InMemoryBook(string name) : base(name)
        {
            grades = new List<double>();
            Name = name;
        }

        public override void AddGrade(double grade)
        {
            if (grade <= 100 && grade >= 0) {
                grades.Add(grade);
                GradeAdded?.Invoke(this, EventArgs.Empty);

            } else {
                throw new ArgumentException($"Invalid {nameof(grade)}");
            }
        }

        public void AddGrade(char letter) {
            switch (letter)
            {
                case 'A':
                    AddGrade(90);
                    break;
                case 'B':
                    AddGrade(80);
                    break;
                case 'C':
                    AddGrade(70);
                    break;
                default:
                    AddGrade(0);
                    break;
            }
        }

        public override event GradeAddedDelegate GradeAdded;
        public override Statistics GetStatistics() {
            var result = new Statistics
            {
                HighGrade = double.MinValue,
                LowGrade = double.MaxValue
            };

            foreach (var grade in grades) {
                result.HighGrade = Math.Max(grade, result.HighGrade);
                result.LowGrade = Math.Min(grade, result.LowGrade);
                result.Average += grade;
            }
            result.Average /= grades.Count;

            result.Letter = result.Average switch
            {
                var d when d >= 90.0 => 'A',
                var d when d >= 80.0 => 'B',
                var d when d >= 70.0 => 'C',
                var d when d >= 60.0 => 'D',
                _ => 'F',
            };
            return result;
        }

        private readonly List<double> grades;
        public const string CATEGORY = "Science";
    }

    public class DiskBook : Book
    {
        public DiskBook(string name) : base(name)
        {
        }

        public override event GradeAddedDelegate GradeAdded;

        public override void AddGrade(double grade) {
            string path = $"../{Name}.txt";
            using StreamWriter sw = File.CreateText(path);

            if (!File.Exists(path))
            {
                File.Create(path);
                TextWriter tw = new StreamWriter(path);
                tw.WriteLine(grade);
                tw.Close();
            }
            else if (File.Exists(path))
            {
                using var tw = new StreamWriter(path, true);
                tw.WriteLine(grade);
            }
        }

        public override Statistics GetStatistics()
        {
            throw new NotImplementedException();
        }
    }
}

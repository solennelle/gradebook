﻿using System;
using System.Collections.Generic;

namespace GradeBook
{
    public static class Program
    {
        private static void Main()
        {
            IBook book = new DiskBook("My grade Book");
            book.GradeAdded += OnGradeAdded;

            EnterGrade(book);
            Statistics result = book.GetStatistics();

            Console.WriteLine($"The lowest grade is {result.LowGrade}");
            Console.WriteLine($"The highest grade is {result.HighGrade}");
            Console.WriteLine($"The average grade is {result.Average:N1}");
            Console.WriteLine($"The grade is {result.Letter}");
        }

        private static void EnterGrade(IBook book)
        {
            while (true)
            {
                Console.WriteLine("Enter a grade or 'q' to quit");
                var input = Console.ReadLine();
                if (input == "q")
                {
                    break;
                }
                try
                {
                    var grade = double.Parse(input);
                    book.AddGrade(grade);
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    Console.WriteLine("***");
                }
            }
        }

        private static void OnGradeAdded(object sender, EventArgs args) {
            Console.WriteLine("A grade was added");
        }
    }
}
